#!/bin/bash

source ssshtest

STOP_ON_FAIL=0

echo "Test 1"
run test_in_stdout python3 -c "print('example assert_in_stdout success')"
assert_in_stdout "example"

echo "Test 2"
run test_in_stderr python3 -c "import sys; sys.stderr.write('example assert_in_stdout failure')"
assert_in_stderr "example"

echo "Test3"
assert_equal 42 $((21 + 21))

echo "Test 4"
run test_exit_code python3 -c "print(1);"
assert_exit_code $EX_OK

echo "Test 5"
touch test.out
run test_assert_equal python3 -c "f = open('test.out','w'); f.write('1\n2\n3\n');f.close();"
assert_equal "$(cat test.out | wc -l)" 3
assert_equal "$(cat test.out | wc -l | awk '{print $1;}')" 3
rm -f test.out

echo "Test 6"
touch test.out
run test_assert_equal python3 -c "f = open('test.out','w'); f.write('1\n2\n3\n');f.close();"
assert_equal "$(cat test.out | wc -l)" 3
assert_equal "$(cat test.out | wc -l | awk '{print $1;}')" 3
rm test.out
